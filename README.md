# ckanext-aghna_theme

CKAN extension for [data.ciamiskab.go.id](https://data.ciamiskab.go.id)(ckanext-aghna-theme)

## Installation: To install the plugin, enter your virtualenv and load the source (into the /src directory where you installed CKAN):
```
$ git clone https://github.com/alesservin/ckanext-aghna_theme.git
cd ckanext-aghna_theme
(pyenv)$ python setup.py develop
```
This will also register a plugin entry point, so you now should be able to add the following to your CKAN .ini file:

Add datos_theme to the .ini file.
```
ckan.plugins = <OTHER_PLUGINS> aghna_theme
```
Apply these changes to the .ini file.
```
ckan.site_logo = /base/images/aghna.jpg
```

Note: The static pages app.html, changelog.html, faq.html and retos.html are in ckanext/aghna_theme/public

# Run CKAN (installed from source) locally
Execute:

```
. /usr/lib/ckan/default/bin/activate
ckan -c /etc/ckan/default/ckan.ini run
```
